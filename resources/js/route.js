import Vue from 'vue'
import VueRouter from 'vue-router'
import TopicPostsList from "./components/TopicPostsList";
import PostList from "./layouts/Posts";
import PostItem from './components/PostItem'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'index',
        component: PostList,
    },
    {
        path: '/post/:id',
        name: 'post',
        component:  PostItem
    },
    {
        path: '/topics/:slug',
        name: 'topic',
        component: TopicPostsList
    }
]
export default new VueRouter({
    mode: 'history',
    routes
})
