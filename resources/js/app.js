import Vue from 'vue'
import VueRouter from 'vue-router'
import router from './route'
import './bootstrap'
import ApolloClient from 'apollo-boost'
import VueApollo from 'vue-apollo'
import Main from "./Main"

window.Vue = Vue
Vue.use(VueRouter)



Vue.use(VueApollo)
const apolloClient = new ApolloClient({
    uri: 'http://127.0.0.1:8000/graphql'
})

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
})


const app = new Vue({
    el: '#app',
    apolloProvider,
    router,
    template: '<Main/>',
    components: {
        Main
    }
});
